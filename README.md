# Vizzuality VIIRS Assignnment [Hubert Thieriot]

Assignment for Vizzuality Scientist position.


### Installing

#### Getting Conda
[Anaconda](https://www.anaconda.com/distribution/) -or- [Miniconda](https://docs.conda.io/en/latest/miniconda.html)

#### Creating environment
```
conda env create -f environment.yml
conda activate vizz-viirs 
```

#### Launching notebook
```buildoutcfg
jupyter notebook vizz_viirs.ipynb --NotebookApp.iopub_data_rate_limit=1.0e10
```

### Results
See [screenshot](../tree/master/screenshot.)